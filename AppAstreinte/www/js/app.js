// Exclusion des variables globales non définies
/*global $:false, document:false, window:false, console:false, alert:false, cordova:false, FileReader:false, LocalFileSystem:false, location:false*/

// Récupération du chemin du dossier de stockage et des fichiers xml
function gotFS(fileSystem) {

    // Active le mode strict
    'use strict';

    // Variables
    var nbrResults = document.getElementById('nbrResults'),
        directoryEntry = fileSystem.root;

    // Récupération de global.xml
    directoryEntry.getFile("global.xml", {create: false, exclusive: false}, function (fileEntry) {

        // URL du fichier
        window.global = fileEntry.toURL();

        // Récupération des metadata du fichier
        fileEntry.file(function(file) {

            // Date de la dernière modification
            var dateMAJ = "";

            dateMAJ += (new Date(file.lastModifiedDate));
            document.querySelector("#maj").innerHTML += '<br>' + dateMAJ;
            console.dir(file);

            // Comparaison avec la date de MAJ
            var dateNow = new Date();

            if (dateMAJ +7 > dateNow) {
                alert('le fichier \"global\" date du : ' + dateMAJ + ', merci de le mettre à jour.');
            } else {
                console.log(dateMAJ);
            }
        });

       // On affiche le message suivant si le fichier n'existe pas
    }, function () {
        nbrResults.innerHTML += "Mise à jour du ficher \"global\" nécessaire<br>";
    });

    // Récupération de contacts.xml
    directoryEntry.getFile("contacts.xml", {create: false, exclusive: false}, function (fileEntry) {

        // URL du fichier
        window.contact = fileEntry.toURL();

        // Récupération des metadata du fichier
        fileEntry.file(function(file) {

            // Date de la dernière modification
            var dateMAJ = "";

            dateMAJ += (new Date(file.lastModifiedDate));
            console.dir(file);

            // Comparaison avec la date de MAJ
            var dateNow = new Date();

            if (dateMAJ +7 > dateNow) {
                alert('le fichier \"contacts\" date du : ' + dateMAJ + ', merci de le mettre à jour.');
            } else {
                console.log(dateMAJ);
            }
        });

       // On affiche le message suivant si le fichier n'existe pas
    }, function () {
        nbrResults.innerHTML += "Mise à jour du fichier \"contacts\" nécessaire<br>";
    });
}

// Gestion des ereurs (gotFS)
function fail(e) {

    // Active le mode strict
    'use strict';

    // (Liste des erreurs et leur significations : https://github.com/apache/cordova-plugin-file#list-of-error-codes-and-meanings)
    alert('Impossible de mettre les fichiers à jour, code d\'erreur n°: ' + e.code);
}

// Requis pour le traitement des fichiers, cette fonction écoute l'état du device, rend les changements persistants et déclenche les callbacks
document.addEventListener('deviceready', function() {
    window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
}, false);

// Fonction principale déclenchée par le bouton de recherche
$(document).on('click', '#btnRecherche', function () {

    // Active le mode strict
    'use strict';

    // Déclaration des éléments du DOM utilisé par app.js
    var searchResult = document.createElement("div"),
        results = document.getElementById('results'),
        resultsPresta = document.getElementById('resultsPresta'),
        resultsContacts = document.getElementById('resultsContacts'),
        resultsSites = document.getElementById('resultsSites'),
        nbrResults = document.getElementById('nbrResults'),
        divDetail = document.getElementById('divDetail');

    // Vide les différentes div et leurs enfants lors d'une nouvelle recherche
    while (results.firstChild) { results.removeChild(results.firstChild); }
    while (resultsPresta.firstChild) { resultsPresta.removeChild(resultsPresta.firstChild); }
    while (resultsContacts.firstChild) { resultsContacts.removeChild(resultsContacts.firstChild); }
    while (resultsSites.firstChild) { resultsSites.removeChild(resultsSites.firstChild); }
    while (nbrResults.firstChild) { nbrResults.removeChild(nbrResults.firstChild); }

    // Variables utilisées pour la recherche et la création des panels de l'accordéon
    var searchFor = $('#search').val(),
        reg = new RegExp(searchFor, "i"),
        hash = 0,
        template = $(".template");

    // Affiche le bouton "Consulter" et cache le système d'onglet
    $('#btnDetail').show();
    $('.tab').hide();

    // Verifie la présence d'un terme de recherche contenant au moins 3 lettres
    if (searchFor.length >= 3) {

        // Appel Ajax vers le fichier global.xml
        $.ajax({
            type: "GET",
            url : "xml/MOBILITE_GLOBAL.xml", // L'utilisation du fichier XML est nécessaire pour une utilisation en local. Une variable 'window.global' est cependant disponible.
            dataType: "xml",

            // Callback pour modifier le contenu de nbrResults
            beforeSend: function () {
                nbrResults.innerHTML = 'Chargement...';
            },

            // Fonction de recherche appelée si la requête réussie
            success: function (search) {

                // Boucle sur les différentes balises <INFOS>
                $(search).find('INFOS').each(function () {

                    // Récupère les informations relatives aux sites avec leurs regEX (ESI_REF_OI, ESI_ADR, ESI_COM)
                    var ESI_ADR = $(this).find('ESI_ADR').text(),
                        ESI_ADRSearch = ESI_ADR.search(reg),
                        ESI_COM = $(this).find('ESI_COM').text(),
                        ESI_COMSearch = ESI_COM.search(reg);

                    // Comparaison de la recherche avec les éléments du XML
                    if ((ESI_COM === searchFor) || (ESI_ADR === searchFor) || (ESI_COMSearch > -1) || (ESI_ADRSearch > -1)) {

                        // Chargement des informations relatives aux sites trouvés
                        var ESI_REF_OI = $(this).find('ESI_REF_OI').text(),
                            ESI_NAT = $(this).find('ESI_NAT').text(),
                            ID = $(this).find('ID').text(),
                            GROUPE = $(this).find('NOM').text(),
                            ESI_CODPOS = $(this).find('ESI_CODPOS').text(),
                            NBRLOGT = $(this).find('NBRLOGT').text(),
                            TERRITOIRE = $(this).find('TERRITOIRE').text();

                        // Remplissage de la div "searchResult" avec les informations correspondantes
                        searchResult.innerHTML = ("<b>ESI :</b> " + ESI_REF_OI + "<br>" +
                                                  "<b>Nature :</b> " + ESI_NAT + "<br>" +
                                                  "<b>Nombre de logement :</b> " + NBRLOGT + "<br>" +
                                                  "<b>Territoire :</b> " + TERRITOIRE + "<br>" +
                                                  "<b>Adresse :</b> " + ESI_COM + ", " + ESI_ADR + ", " + ESI_CODPOS);

                        // Propriétés de "searchResult"
                        var array = [ID];
                        searchResult.className = "detail";
                        searchResult.id = array;

                        // Placement de "searchResult"
                        var childDivDetail = divDetail.firstChild;
                        divDetail.insertBefore(searchResult, childDivDetail);

                        // Création du clone de template
                        var newPanel = template.clone();

                        // Affiche PAVI en fonction de la nature de l'ESI
                        if (ESI_NAT === "PAVI") {
                            newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(GROUPE + " PAVI");
                        } else {
                            newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(GROUPE);
                        }

                        // Attribution des paramètres
                        while (divDetail.firstChild) { divDetail.removeChild(divDetail.firstChild); }
                        newPanel.find(".panel-collapse").attr("id", hash);
                        $("#results").append(newPanel.fadeIn());
                    }
                });

                // Affichage des détails déclanchée par le bouton "Consulter"
                $(document).on('click', '#btnDetail', function () {

                    // Affiche les onglets et cache le bouton "Consulter"
                    $('#btnDetail').hide();
                    $('.tab').show();

                    // Vide les différentes div et leurs enfants lors de la consultation d'un site
                    while (results.firstChild) { results.removeChild(results.firstChild); }
                    while (resultsPresta.firstChild) { resultsPresta.removeChild(resultsPresta.firstChild); }
                    while (resultsContacts.firstChild) { resultsContacts.removeChild(resultsContacts.firstChild); }
                    while (resultsSites.firstChild) { resultsSites.removeChild(resultsSites.firstChild); }
                    while (nbrResults.firstChild) { nbrResults.removeChild(nbrResults.firstChild); }

                    // Création des différentes div contenants les informations du site
                    var prestataires = document.createElement("div"),
                        sites = document.createElement("div"),
                        esiContacts = document.createElement("div"),
                        IDChild = $(this).prev('div').children('.detail').attr('id');

                    // Récupération des informations du site
                    $(search).find('INFOS').each(function () {

                        // Comparaison de IDChild avec les informations du site
                        if (IDChild === $(this).find('ID').text()) {

                            // Chargement des informations relatives au site
                            var ESI_REF_OI = $(this).find('ESI_REF_OI').text(),
                                GROUPE = $(this).find('NOM').text(),
                                ESI_ADR = $(this).find('ESI_ADR').text(),
                                ESI_CODPOS = $(this).find('ESI_CODPOS').text(),
                                ESI_COM = $(this).find('ESI_COM').text(),
                                ESI_NAT = $(this).find('ESI_NAT').text(),
                                NBRLOGT = $(this).find('NBRLOGT').text(),
                                TERRITOIRE = $(this).find('TERRITOIRE').text();

                            // Remplissage de la div "searchResult" avec les informations correspondantes
                            searchResult.innerHTML = ("<b>ESI :</b> " + ESI_REF_OI + "<br>" +
                                                      "<b>Nature :</b> " + ESI_NAT + "<br>" +
                                                      "<b>Nombre de logement :</b> " + NBRLOGT + "<br>" +
                                                      "<b>Territoire :</b> " + TERRITOIRE + "<br>" +
                                                      "<b>Adresse :</b> " + ESI_COM + ", " + ESI_ADR + ", " + ESI_CODPOS);

                            // Propriétés de "searchResult"
                            searchResult.className = "detail";
                            searchResult.id = IDChild;

                            // Placement de "searchResult"
                            sites = document.getElementById('divDetail');
                            var childsites = sites.firstChild;
                            sites.insertBefore(searchResult, childsites);

                            // Création du clone de template et attribution des paramètres
                            var newPanel = template.clone();

                            // Affiche PAVI en fonction de la nature de l'ESI
                            if (ESI_NAT === "PAVI") {
                                newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(GROUPE + " PAVI");
                            } else {
                                newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(GROUPE);
                            }

                            // Attribution des paramètres
                            while (sites.firstChild) { sites.removeChild(sites.firstChild); }
                            newPanel.find(".collapse").removeClass("collapse");
                            newPanel.find(".panel-collapse").attr("id", hash);
                            $("#resultsSites").append(newPanel.fadeIn());
                        }
                    });

                    // Récupération des informations contacts liées au site
                    $(search).find('CONTACT').each(function () {

                        // Comparaison de IDChild avec l'ID du contact
                        if (IDChild === $(this).find('ID').text()) {

                            // Chargement des informations relatives aux contacts
                            var CAT = $(this).find('CAT').text(),
                                NOM = $(this).find('NOM').text(),
                                PRENOM = $(this).find('PRENOM').text(),
                                TEL1_CONTACT = $(this).find('TEL1').text(),
                                TEL2_CONTACT = $(this).find('TEL2').text(),
                                EMAIL = $(this).find('EMAIL').text(),
                                esiContactsContent = document.getElementById('divDetail'),
                                childesiContactsContent = esiContactsContent.firstChild;

                            // Remplissage de la div "esiContacts" avec les informations correspondantes
                            esiContacts.innerHTML = ("<b> Nom : </b>" + NOM + "<br>" +
                                                     "<b> Prenom : </b>" + PRENOM + "<br>" +
                                                     "<b>Telephone 1 :</b> " + "<a href=tel:" + TEL1_CONTACT + ">" + TEL1_CONTACT + "</a> <br>" +
                                                     "<b>Telephone 2 :</b> " + "<a href=tel:" + TEL2_CONTACT + ">" + TEL2_CONTACT + "</a> <br>" +
                                                     "<b> Email :</b> " + "<a href=mailto:" + EMAIL +
                                                     "?subject=Problème%20avec%20l'OI%20" + IDChild +
                                                     "&amp;body=Description%20du%20problème%20:>" + EMAIL + "</a>");

                            // Propriétés de "esiContacts"
                            esiContacts.className = "esiContacts";
                            esiContacts.id = CAT;

                            // Placement de "esiContacts"
                            esiContactsContent.insertBefore(esiContacts, childesiContactsContent);

                            // Création du clone de template et attribution des paramètres
                            var newPanel = template.clone();

                            // Attribution des paramètres
                            newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(CAT);
                            while (esiContacts.firstChild) { esiContacts.removeChild(esiContacts.firstChild); }
                            newPanel.find(".panel-collapse").attr("id", hash);
                            $("#resultsContacts").append(newPanel.fadeIn());
                        }
                    });

                    // Récupération des informations elus liées au site
                    $(search).find('ELUS').each(function () {

                        // Comparaison de IDChild avec l'attribut "ID" de la balise "ELUS"
                        if (IDChild === $(this).attr("ID")) {

                            // Pour chaque résultat
                            $(search).find('ELU').each(function () {

                              // Comparaison de IDChild avec l'ID de l'elu
                                if (IDChild === $(this).find('ID').text()) {

                                    // Chargement des informations relatives aux elus
                                    var NOM = $(this).find('NOM').text(),
                                        PRENOM = $(this).find('PRENOM').text(),
                                        TEL1_CONTACT = $(this).find('TEL1').text(),
                                        TEL2_CONTACT = $(this).find('TEL2').text(),
                                        EMAIL = $(this).find('EMAIL').text(),
                                        esiContactsContent = document.getElementById('divDetail'),
                                        childesiContactsContent = esiContactsContent.firstChild;

                                    // Remplissage de la div "esiContacts" avec les différentes informations
                                    esiContacts.innerHTML += ("<b> Nom : </b>" + NOM +
                                                              "<br>" + "<b> Prenom : </b>" + PRENOM +
                                                              "<br>" + "<b>Telephone 1 :</b> " + "<a href=tel:" + TEL1_CONTACT + ">" + TEL1_CONTACT +
                                                              "</a> <br>" + "<b>Telephone 2 :</b> " + "<a href=tel:" + TEL2_CONTACT + ">" + TEL2_CONTACT +
                                                              "</a> <br>" + "<b> Email :</b> " + "<a href=mailto:" + EMAIL + "?subject=Problème%20avec%20l'OI%20" + IDChild +
                                                              "&amp;body=Description%20du%20problème%20:>" + EMAIL + "</a><br><br>");

                                    // Propriétés de "esiContacts"
                                    esiContacts.className = "esiContacts";
                                    esiContacts.id = "Elus";

                                    // Placement de "esiContacts"
                                    esiContactsContent.insertBefore(esiContacts, childesiContactsContent);
                                }
                            });

                            // Création du clone de template et attribution des paramètres
                            var newPanel = template.clone();

                            // Attribution des paramètres
                            newPanel.find(".accordion-toggle").attr("href", "#" + (++hash)).text("Elus");
                            while (esiContacts.firstChild) { esiContacts.removeChild(esiContacts.firstChild); }
                            newPanel.find(".panel-collapse").attr("id", hash);
                            $("#resultsContacts").append(newPanel.fadeIn());
                        }
                    });

                    // Récupération des informations contacts liés aux prestataires
                    $(search).find('ASSOCIATION').each(function () {

                        // Comparaison de IDChild avec l'ID du préstataire
                        if (IDChild === $(this).find('ID').text()) {

                            // Chargement des informations relatives aux préstataires
                            var MOT_LIB = $(this).find('MOT_LIB').text(),
                                ORDRE = $(this).find('ORDRE').text(),
                                NOM_PRESTA = $(this).find('NOM_PRESTA').text(),
                                TEL1_PRESTA = $(this).find('TEL1').text(),
                                TEL2_PRESTA = $(this).find('TEL2').text(),
                                INFO_COMPL = $(this).find('INFO_COMPL').text(),
                                prestaContent = document.getElementById('divDetail'),
                                childprestaContent = prestaContent.firstChild;

                            // Remplissage de la div "prestataires" avec les différentes informations
                            prestataires.innerHTML = ("<b>Prestaraire : </b> " + NOM_PRESTA + "<br> " +
                                                      "<b>Informations complémentaires :</b> " + INFO_COMPL + "<br>" +
                                                      "<b>Telephone 1 :</b> " + "<a href=tel:" + TEL1_PRESTA + ">" + TEL1_PRESTA + "</a> <br>" +
                                                      "<b>Telephone 2 :</b> " + "<a href=tel:" + TEL2_PRESTA + ">" + TEL2_PRESTA + "</a> <br>");

                            // Propriétés de "prestataires"
                            prestataires.className = "prestataire";
                            prestataires.id = MOT_LIB;

                            // Placement de "prestataires"
                            prestaContent.insertBefore(prestataires, childprestaContent);

                            // Création du clone de template et attribution des paramètres
                            var newPanel = template.clone();

                            // Récupère l'ordre des prestataires, une amélioration consite à regrouper les MOT_LIB dans un seul clone de template (Voir ELUS)
                            if (ORDRE === '1') {
                                newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(MOT_LIB);
                            } else {
                                newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(MOT_LIB + " " + ORDRE);
                            }

                            // Attribution des paramètres
                            while (prestaContent.firstChild) { prestaContent.removeChild(prestaContent.firstChild); }
                            newPanel.find(".panel-collapse").attr("id", hash);
                            $("#resultsPresta").append(newPanel.fadeIn());

                        }
                    });

                    // Bouton mail "Signaler un problème"
                    $(document).on('click', '#btnMail', function () {

                        // Destinataire, message, sujet + n°ESI et lien
                        var recipient = "application_astreinte@habitat76.fr",
                            msgText = $('#message-text').val(),
                            link = ('href', 'mailto:' + recipient + '?subject=' + encodeURIComponent("Problème avec la fiche astreine : " + IDChild) + '&body=' + encodeURIComponent(msgText));

                        // Ouvre une nouvelle fenêtre
                        window.location.href = link;
                    });
                });
            },

            // Affiche le nombre de résultat obtenu
            complete: function () {
                nbrResults.innerHTML = $(".accordion-toggle").length - 1 + " résultats pour : " + searchFor;
            }
        });

    } else { // Si le champ de recherche "searchFor" est vide ou =< 3 on affiche le message suivant
        nbrResults.innerHTML = ("Merci de renseigner un des paramètres suivant : <br><br>" + "<ul><li>Commune</li>" + "<li>Adresse</li></ul>");
    }
});

// Bouton annuaire disponible dans le header
$(document).on('click', '#contacts', function () {

    // Active le mode strict
    'use strict';

    // Déclaration des éléments du DOM utilisé par app.js
    var searchResult = document.createElement("div"),
        results = document.getElementById('results'),
        resultsPresta = document.getElementById('resultsPresta'),
        resultsContacts = document.getElementById('resultsContacts'),
        resultsSites = document.getElementById('resultsSites'),
        nbrResults = document.getElementById('nbrResults'),
        divDetail = document.getElementById('divDetail');

    // Vide les différentes div et leurs enfants lors d'une nouvelle recherche
    while (results.firstChild) { results.removeChild(results.firstChild); }
    while (resultsPresta.firstChild) { resultsPresta.removeChild(resultsPresta.firstChild); }
    while (resultsContacts.firstChild) { resultsContacts.removeChild(resultsContacts.firstChild); }
    while (resultsSites.firstChild) { resultsSites.removeChild(resultsSites.firstChild); }
    while (nbrResults.firstChild) { nbrResults.removeChild(nbrResults.firstChild); }

    // Variables utilisés pour la recherche et la création des panel de l'accordéon
    var searchFor = $('#search').val(),
        hash = 0,
        template = $(".template");

    // Affiche le bouton "Consulter" et cache le système d'onglet
    $('#btnDetail').hide();
    $('.tab').hide();

     // Appel Ajax vers le fichier contact.xml
    $.ajax({
        type: "GET",
        url: "xml/CELLULE_CRISE.xml", // L'utilisation du fichier XML est nécessaire pour une utilisation en local. Une variable 'window.contact' est cependant disponible.
        dataType: "xml",

        // Affiche "Chargement" tant que l'appel Ajax n'est pas terminé
        beforeSend: function () {
            nbrResults.innerHTML = 'Chargement...';
        },

        // Fonction de recherche
        success: function (search) {

            // Boucle sur toutes les balises <CONTACT>
            $(search).find('CONTACT').each(function () {

                // Chargement des informations relatives aux contacts
                var PRENOM = $(this).find('PRENOM').text(),
                    NOM = $(this).find('NOM').text(),
                    SERVICE = $(this).find('SERVICE').text(),
                    TEL1_CONTACT = $(this).find('TEL1').text(),
                    TEL2_CONTACT = $(this).find('TEL2').text(),
                    EMAIL = $(this).find('EMAIL').text();

                // Remplissage de la div "esiContacts" avec les différentes informations
                searchResult.innerHTML = ("<b> Nom : </b>" + NOM + "<br>" +
                                         "<b> Prenom : </b>" + PRENOM + "<br>" +
                                         "<b> Service : </b>" + SERVICE + "<br>" +
                                         "<b>Telephone 1 :</b> " + "<a href=tel:" + TEL1_CONTACT + ">" + TEL1_CONTACT + "</a> <br>" +
                                         "<b>Telephone 2 :</b> " + "<a href=tel:" + TEL2_CONTACT + ">" + TEL2_CONTACT + "</a> <br>" +
                                         "<b> Email :</b> " + "<a href=mailto:" + EMAIL +
                                         "?subject=Problème%20avec%20un%20OI%20" +
                                         "&amp;body=Description%20du%20problème%20:>" + EMAIL + "</a>");

                 // Propriétés de "searchResult"
                searchResult.className = "detail";
                searchResult.id = NOM;

                // Placement de "searchResult"
                var childDivDetail = divDetail.firstChild;
                divDetail.insertBefore(searchResult, childDivDetail);

                // Création du clone de template
                var newPanel = template.clone();

                newPanel.find(".accordion-toggle").attr("href",  "#" + (++hash)).text(SERVICE);

                // Attribution des paramètres
                while (divDetail.firstChild) { divDetail.removeChild(divDetail.firstChild); }
                newPanel.find(".panel-collapse").attr("id", hash);
                $("#results").append(newPanel.fadeIn());
            });
        },

        // Affiche "Annuaire :"
        complete: function () {
            nbrResults.innerHTML = "Annuaire :";
        }
    });
});

// Bouton mise à jour disponible dans le header
$(document).on('click', '#maj', function () {

    // Active le mode strict
    'use strict';

    // Définit nbrResults pour une utilisation furur : (Voir :  http://tinyurl.com/2fcpre6)
    var nbrResults = document.getElementById('nbrResults');

    // Permet de faire plusieurs appels Ajax asynchrones (Voir : https://api.jquery.com/jquery.when/)
    $.when(

        // Appel Ajax vers le serveur extra pour creer le fichier global.xml
        $.ajax({
            type: "GET",
            url: "https://extra.habitat76.fr/astreinte/GetFluxXmlServlet",
            dataType: "text",

            // Callback pour modifier le contenu de nbrResults
            beforeSend: function () {
                nbrResults.innerHTML = 'Chargement...<br>';
            },

            // Fonction de mise à jour
            success: function (MAJ) {

                // MAJ représente les données brut, content représente un string
                var content = MAJ;

                // Ecoute l'état du device et verifie la présence du fichier
                document.addEventListener("deviceready", checkIfFileExists, false);

                // Rend les changements persistents et détermine si le fichier global.xml existe
                function checkIfFileExists() {
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                        fileSystem.root.getFile("global.xml", { create: false }, fileExists, fileDoesNotExist);
                    }, getFSFail);
                }

                // Si le fichier existe on le modifie, si il n'existe pas on le creer
                function fileExists(fileEntry) {
                    fileEntry.createWriter(truncateFile, fileDoesNotExist);
                }

                // Permet de tronquer l'ancien fichier et d'écrire par dessus
                function truncateFile(writer) {
                    writer.onwriteend = function (evt) {
                        console.log("Writer" + evt.code);
                        writer.seek(0);
                        writer.onwriteend = function (evt) {
                            console.log("Changed" + evt.code);
                        };
                        writer.write(content);
                    };
                    writer.truncate(0);
                }

                // Si le fichier n'existe pas création puis écriture
                function fileDoesNotExist() {

                    // Récupere l'état du device
                    document.addEventListener("deviceready", onDeviceReady, false);

                    // Rend les changements persistant et appelle les callbacks
                    function onDeviceReady() {
                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess, onError);
                    }

                    // Utilise un objet fileSystem pour travailler sur la création
                    function onSuccess(fileSystem) {

                        // Objet fileSystem
                        var directoryEntry = fileSystem.root;

                        // Création du fichier global.xml et écriture des données
                        directoryEntry.getFile("global.xml", {create: true, exclusive: false}, function (fileEntry) {
                            fileEntry.createWriter(function (writer) {
                                writer.write(content);
                            }, function (e) {
                                console.log("Error :" + e.code);
                            });
                        }, function (e) {
                            console.log("Error :" + e.code);
                        });
                    }

                    // Gestion des erreurs
                    function onError(evt) {
                        console.log("Error :" + evt.code);
                    }
                }

                // Callback en cas d'echec de la fonction onSuccess
                function getFSFail(evt) {
                    console.log(evt.target.error.code);
                }
            },

            // Informe l'utilisateur de l'état de la mise à jour
            complete: function () {
                nbrResults.innerHTML = 'Mise à jour réusie';
            }
        }),

        // Deuxieme ppel Ajax vers le serveur extra pour creer le fichier contacts.xml
        $.ajax({
            type: "GET",
            url: "https://extra.habitat76.fr/astreinte/GetFluxXmlServlet?fic=CCRISE",
            dataType: "text",

            // Callback pour modifier le contenu de nbrResults
            beforeSend: function () {
                nbrResults.innerHTML = 'Chargement...<br>';
            },

            // Fonction de mise à jour
            success: function (MAJ) {

                // MAJ représente les données brut, content représente un string
                var content = MAJ;

                // Ecoute l'état du device et verifie la présence du fichier
                document.addEventListener("deviceready", checkIfFileExists, false);

                // Rend les changements persistents et détermine si le fichier global.xml existe
                function checkIfFileExists() {
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                        fileSystem.root.getFile("contacts.xml", { create: false }, fileExists, fileDoesNotExist);
                    }, getFSFail);
                }

                // Si le fichier existe on le modifie, si il n'existe pas on le creer
                function fileExists(fileEntry) {
                    fileEntry.createWriter(truncateFile, fileDoesNotExist);
                }

                // Permet de tronquer l'ancien fichier et d'écrire par dessus
                function truncateFile(writer) {
                    writer.onwriteend = function (evt) {
                        console.log("Writer" + evt.code);
                        writer.seek(0);
                        writer.onwriteend = function (evt) {
                            console.log("Changed" + evt.code);
                        };
                        writer.write(content);
                    };
                    writer.truncate(0);
                }

                // Si le fichier n'existe pas création puis écriture
                function fileDoesNotExist() {

                    // Récupere l'état du device
                    document.addEventListener("deviceready", onDeviceReady, false);

                    function onDeviceReady() {
                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess, onError);
                    }

                    // Utilise un objet fileSystem pour travailler sur la création
                    function onSuccess(fileSystem) {

                        // Objet fileSystem
                        var directoryEntry = fileSystem.root;

                        // Création du fichier contacts.xml et écriture des données
                        directoryEntry.getFile("contacts.xml", {create: true, exclusive: false}, function (fileEntry) {

                            fileEntry.createWriter(function (writer) {
                                writer.write(content);
                            }, function (e) {
                                console.log("Error :" + e.code);
                            });
                        }, function (e) {
                            console.log("Error :" + e.code);
                        });
                    }

                    // Gestion des erreurs
                    function onError(evt) {
                        console.log("Error :" + evt.code);
                    }
                }

                // Callback en cas d'echec de la fonction onSuccess
                function getFSFail(evt) {
                    console.log(evt.target.error.code);
                }
            },

            // Informe l'utilisateur de l'état de la mise à jour
            complete: function () {
                nbrResults.innerHTML = 'Mise à jour réusie';
            }
        })
    ).

    // Recharge la page si les deux appels Ajax ont réussis
    then( function(){
        window.location.reload();
    });
});

// Bouton btnItineraire
$(document).on('click', '#btnItineraire', function () {

    'use strict';

    alert('Disponible prochainement');
});

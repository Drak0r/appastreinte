# Application Astreinte #

Le but du projet "Astreinte" est de faciliter les prises de contacts entre les cadres d'astreinte et les intervenants lors des permanences. 

Ce projet intègre un certain nombre de données,cependant seul le terme "**Rouen**" est utilisable pour cette démo.

### Utilisation ###

* Possibilité de build avec le Intel XDK ou la CLI de Cordova
* Lancement de l'application dans ../www/index.html
* Sous Firefox pressez CTRL+SHIFT+M pour obtenir une vue responsive 

### Précisions ###

* Le build de l'application n'est nécessaire que pour l'API "Files" de Cordova